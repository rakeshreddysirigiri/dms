package com.te.dms.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.te.dms.helper.FileUploadHelper;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping(path="/user")
public class FileUploadController {

	private final FileUploadHelper fileUploadHelper;
	
	@PostMapping(path="/upload-file")
	public ResponseEntity<String> uploadFile(@RequestParam("rakesh") MultipartFile file){
		Boolean f = fileUploadHelper.uploadFile(file); 
		if(f) {
			return ResponseEntity.ok("File has been uploaded succesfully");
		}
		return ResponseEntity.ok("unable to upload the file"); 
	}
	@GetMapping(path="/download/{filename}")
	public ResponseEntity<Object> downloadFile(@PathVariable("filename") String fileName) throws FileNotFoundException{
		String responseFile= "C:\\Users\\rajes\\OneDrive\\Desktop\\uplift-hyd-2022\\project-2\\document-management-system\\src\\main\\resources\\static\\documents\\"+fileName;
		File file = new File(responseFile);
		System.out.println(fileName);
		InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition",String.format("attatchment;filename=\"%s\"", file.getName()));
		headers.add("Cache-Control","no-cache,no-store,must-revalidate");
		headers.add("Pragma","no-cache");
		headers.add("Expires","0");
		ResponseEntity<Object> responseEntity = ResponseEntity.ok().headers(headers).contentType(MediaType.parseMediaType("application/txt")).body(resource);
		return responseEntity;
	}
	
	@PutMapping(path="/rename/{renamefrom}/{renameto}")
	public ResponseEntity<String> renameFile(@PathVariable("renamefrom") String oldFileName,@PathVariable("renameto") String newFileName) throws IOException{
		
		Path path1=Paths.get("C:\\Users\\rajes\\OneDrive\\Desktop\\uplift-hyd-2022\\project-2\\document-management-system\\src\\main\\resources\\static\\documents\\"+oldFileName);
		Path path2=Paths.get("C:\\Users\\rajes\\OneDrive\\Desktop\\uplift-hyd-2022\\project-2\\document-management-system\\src\\main\\resources\\static\\documents\\"+newFileName);
		Files.move(path1, path2);
		return ResponseEntity.ok("File has been renamed Succesfully");
	}
	
}
