package com.te.dms.service;

public interface EmailSenderService {

	public void sendEmail(String toEmail, String subject, String message);
	
	public void sendMessageWithAttachment(
			  String to, String subject, String text, byte[] value); 
	}
